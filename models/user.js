const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
   firstName: {
      type: String,
      required:[true, "First name is required"]
   },
   lastName: {
      type: String,
      required: [true, "Last name is required"]
   },
   email: {
      type: String,
      required: [true, "Email is required"]
   },
   password: {
      type: String,
      required: [true, "Password is required"]
   },
   isAdmin: {
      type: Boolean,
      default: true
   },
   mobileNo: {
      type: String,
      required: [true, "Number is required"]
   },
   enrollments: [
      {
        CourseId: {
          type: String,
          required: [true, "CourseId is required"]
        },
        enrolledON: {
          type: Date,
          default: new Date()
        },
        status:{
          type: String,
          default: [true, "Status is required"]
        }
      }
   ]
   
  }
)
module.exports = mongoose.model("user", userSchema)
