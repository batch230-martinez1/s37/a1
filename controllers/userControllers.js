const user = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js")
const Course = require("../models/course.js");;

module.exports.registerUser = (reqBody)=>{
  let newUser = new user({
      firstName: reqBody.firstName,
      lastName: reqBody.lastName,
      email: reqBody.email,
      mobileNo: reqBody.mobileNo,
      password: bcrypt.hashSync(reqBody.password, 10)
      //  10 - salt
  })
  return newUser.save().then((user, error)=>{
      if(error){
        return false;
      }
      else{
        return newUser;
      }
  })
}

module.exports.registerUser1 = (reqBody) => {
  return user.findOne({ $or: [{ email: reqBody.email }, { mobileNo: reqBody.mobileNo }] })
    .then(existingUser => {
      if (existingUser) {
       let existingEP = "The email address or phone number has already been registered."
        return existingEP; 
      } 
      else {
        let newUser = new user({
          firstName: reqBody.firstName,
          lastName: reqBody.lastName,
          email: reqBody.email,
          mobileNo: reqBody.mobileNo,
          password: bcrypt.hashSync(reqBody.password, 10)
        });
        return newUser.save().then((user, error) => {
          if (error) {
            return error;
          } else {
            return newUser;
          }
        });
      }
    })
}

//  function checkEmailExist
module.exports.checkEmailExist = (reqBody) =>{
  return user.find({email: reqBody.email}).then(result=>{
      if(result.length > 0){
        return result;
      }
      else{
        return false;
      }
   }
  )
}
module.exports.loginUser = (reqBody) => {
	return user.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			// compareSync is a bcrypt function to compare unhashed password to hashed password
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password); //true or false

			if(isPasswordCorrect){
				// Let's give the user a token to a access features
				return {access: auth.createAccessToken(result)};
			}
			else{
				// If password does not match, else
				return false;

			}
		}
	})
}



// retrieve details through id
/* module.exports.getProfile = (reqBody) =>{
  return user.findById({_id:reqBody._id}).then(result=>{
    if(result == null){
    return false;
    }
    else{
    // altering password field into empty string
    result.password = '';
    return result;
    }
})
}
 */

// Get user Data
module.exports.getProfile = (request, response) =>{
  const userData = auth.decode(request.headers.authorization);
  console.log
  return user.findById(userData.id).then(result =>{
    result.password = "*****";
    response.send(result);
  })    
}


// enroll feature
module.exports.enroll = async(request, response)=>{
    const userData = auth.decode(request.headers.authorization);
    let courseName = await Course.findById(request.body.courseId).then (result => result.name);
    let newData = {
      // UserId and email will be retrieved from the request header (request header contains the user token)
      userId : userData.id,
      email : userData.email,
      //  Course ID will be retrieved from the request
      courseId: request.body.courseId,
      courseName: courseName
    }
    console.log(newData);
    let isUserUpdated = await user.findById(newData.userId)
    .then(user => {
      user.enrollments.push({
        courseId: newData.courseId,
        courseName: newData.courseName
      })
      return user.save()
    .then(result =>{
      console.log(result);
      return true;
    })
    .catch(error=>{
      console.log(error);
      return false;
    })
  })
    console.log(isUserUpdated);

    let isCourseUpdated = await Course.findById(newData.courseId).then(course =>{
      course.enrollees.push({
        userId: newData.userId,
        email:newData.email
      })
   
    //  course.slots = course.slots - 1;
    course.slots -= 1;
      return course.save()
    .then(result => {
      console.log(result);
      return true
    })
    .catch(error =>{
      console.log(error);
      return false;
    })
  })
  console.log(isCourseUpdated);
  //  Condition will check if the both user and course document has been updated
  // Ternary operator
  (isUserUpdated == true && isCourseUpdated == true)? response.send(true) : response.send(false);
}
  /* if(isUserUpdated == true && isCourseUpdated == true){
    response.send(true);
  }
  else{
    response.send(false)
  } */
 

 // Mini Activity -
		// [1] Create a condition that if the slots is already zero, no deduction of slot will happen and...
		// [2] it should have a response that a user is not allowed to enroll due to no available slots
		// [3] Else if the slot is negative value, it should make the slot value to zero
  
















