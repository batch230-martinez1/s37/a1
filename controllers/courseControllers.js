const mongoose = require("mongoose");
const Course = require("../models/course.js");
const auth = require("../auth.js")



//  Function for adding a course
// 2. Modify the "addCourse" controller method to implement admin  authentication for crating purposes
module.exports.addCourse = (reqBody) =>{
  if(reqBody.isAdmin == true){  
    let newCourse = new Course({
    name: reqBody.course.name,
    description: reqBody.course.description,
    price: reqBody.course.price,
    slots: reqBody.course.slots
  })

  return newCourse.save().then((newCourse, error)=>
  {
    if(error){
        return error;
    }
    else{
      return newCourse;
    }
  })
}
else{
  let message = Promise.resolve('User must be ADMIN to access this functionality');
  return message.then((value) => {return value});
}  
}

// Get all course
module.exports.getAllCourse = () =>{
  return Course.find({}).then(result =>{
    return result;
  })
}

//  Get all Active Courses
module.exports.getActiveCourse = () => {
  return Course.find({isActive: true}).then(result =>{
    return result;
  })
}


//  Get specific course
module.exports.getCourse = (courseId) => {
  return Course.findById(courseId).then(result =>{
    return result;
  })
}

// Updating a course
   // newData.course.name
  // newData.request.body.name
  module.exports.updateCourse = (courseId, newData) => {
    if(newData.isAdmin == true){
      return Course.findByIdAndUpdate(courseId,
        {
          // newData.course.name
          // newData.request.body.name
          name: newData.course.name, 
          description: newData.course.description,
          price: newData.course.price
        }
      ).then((result, error)=>{
        if(error){
          return false;
        }
        return result
      })
    }
    else{
      let message = Promise.resolve('User must be ADMIN to access this functionality');
      return message.then((value) => {return value});
    }
  }

// archive course
module.exports.archiveCourse =(courseId, archive) =>{
  if(archive.isAdmin == true){
    return Course.findByIdAndUpdate(courseId, 
      {
        isActive : archive.course.isActive
      },
      {
        new : true
      })
    .then((result, error)=>{
      if(error){
          return false;
        }
     else{
      return result
     }
    }
    )
    }
    else{
      let message = Promise.resolve('User must be ADMIN to access this functionality');
      return message.then((value) => {return value});
    }
}

/* module.exports.archiveCourse =(courseId, archive) =>{
  if(archive.isAdmin == true){
    return Course.findByIdAndUpdate(courseId)
    .then((archive)=>{
      if(archive.course.isActive= true){      
      return archive.course.isActive = false;
      }
      else{
      return archive.course.isActive = true
      }
    }
    )
    .then((result, error)=>{
        if(error){
          return false;
        }
        return result
      }
      )
    }
    else{
      let message = Promise.resolve('User must be ADMIN to access this functionality');
      return message.then((value) => {return value});
    }
} */
  

